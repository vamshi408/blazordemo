﻿namespace Blazor2021
{
    public enum MaritalStatus
    {
        Married,
        Single,
        Other
    }
}